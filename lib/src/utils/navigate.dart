import 'package:flutter/material.dart';
import 'package:panic_app/src/screens/tutorial/tutorial_page.dart';

void navigateTutorial(context) {
  Navigator.pushReplacement(
    context,
    new MaterialPageRoute(builder: (context) => TutorialPage()),
  );
}
