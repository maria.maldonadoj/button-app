import 'package:flutter/material.dart';
import 'package:panic_app/src/values/strings.dart' show Strings;

class InfoWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => InfoWidgetState();
}

class InfoWidgetState extends State<InfoWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.5,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          ParagraphWidget(),
          // ImageWidget(),
          // ButtonsWidget(),
        ],
      ),
    );
  }
}

////////////////////////////////////////////////////////////////////////////////
// PARAGRAPH WITH INDICATIONS
class ParagraphWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ParagraphWidgetState();
}

class ParagraphWidgetState extends State<ParagraphWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.15,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Text(
        Strings.tutorial.paragraph[0],
        textAlign: TextAlign.center,
        style: TextStyle(color: Colors.white70, fontSize: 16),
      ),
    );
  }
}

////////////////////////////////////////////////////////////////////////////////
// IMAGE OF GUARDIAN
class ImageWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ImageWidgetState();
}

class ImageWidgetState extends State<ImageWidget> {
  @override
  Widget build(BuildContext context) {
    return null;
  }
}

////////////////////////////////////////////////////////////////////////////////
// BUTTONS SKIP AND NEXT
class ButtonsWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ButtonsWidgetState();
  }
}

class ButtonsWidgetState extends State<ButtonsWidget> {
  @override
  Widget build(BuildContext context) {
    return null;
  }
}
