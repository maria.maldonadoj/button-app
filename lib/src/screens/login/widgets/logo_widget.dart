import 'package:flutter/material.dart';
import 'package:panic_app/src/values/assets.dart' show Assets;
import 'package:panic_app/src/values/strings.dart' show Strings;

class LogoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.6,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          LogoImageWidget(),
          LogoTextWidget(),
        ],
      ),
    );
  }
}

class LogoImageWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.5,
      child: Image.asset(
        Assets.images.blueSkyscraper,
        fit: BoxFit.contain,
      ),
    );
  }
}

class LogoTextWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.1,
      padding: EdgeInsets.all(15),
      child: Text(
        Strings.login.phrase,
        style: TextStyle(
          color: Colors.black54,
          fontStyle: FontStyle.italic,
          fontSize: 20,
        ),
      ),
    );
  }
}
