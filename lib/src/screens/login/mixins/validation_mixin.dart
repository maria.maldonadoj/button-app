class ValidationMixin {
  String validateEmail(String email) {
    if (email.isEmpty) return "Ingrese su correo";
    if (!email.contains("@open.pucp.pe")) return "Correo incorrecto";
    return null;
  }

  String validatePassword(String password) {
    if (password.isEmpty) return "Ingrese su contraseña";
    if (password.length <= 5) return "Contraseña muy corta";
    if (password.length >= 50) return "Contraseña muy larga";
    return null;
  }
}
