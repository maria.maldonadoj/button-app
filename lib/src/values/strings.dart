class Strings {
  static final app = "OPEN PÁNICO APP";
  static final login = _Login();
  static final tutorial = _Tutorial();
}

class _Login {
  static String _phrase = "SIEMPRE ALERTA";
  static String _connected = "¡Listo!";
  static String _notAllowed = "No tienes permisos para usar este servicio";
  
  static String _hintEmail = "usuario@open.pucp.pe";
  static String _labelEmail = "Correo electrónico";
  static String _labelPassword = "Contraseña";
  static String _signin = "Iniciar sesión";

  get phrase => _phrase;
  get connected => _connected;
  get notAllowed => _notAllowed;

  get hintEmail => _hintEmail;
  get labelEmail => _labelEmail;
  get labelPassword => _labelPassword;
  get signin => _signin;
}

class _Tutorial {
  static final List<String> _paragraph = [
    "Cada vez que un botón de pánico sea presionado, se lanzará " +
        "una notificación como la siguiente:",
    "Cuando no te encuentres demtro de la app, este emitirá un sonido de " +
        "alarma y vibrará por 5 segundos.",
    "Para detener el sonido y la vibración, ingresa a la app por medio " +
        "de la notificación, o presiona el botón de ENTENDIDO dentro de ésta."
  ];

  static String _name = "None";
  static final String _skip = "SKIP";
  static final String _next = "NEXT";
  static final String _finish = "FINISH";

  set name(String value) => _name = value;
  get skip => _skip;
  get next => _next;
  get finish => _finish;
  get paragraph => _paragraph;
  get welcome => "Hola, ${_Tutorial._name}\nBienvenido";
}
