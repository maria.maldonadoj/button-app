class Assets {
  static final images = _Images();
}

class _Images {
  static final _route = "assets/images";

  static final String _user = "$_route/default_user.png";
  static final String _notification = "$_route/notification.png";
  static final String _vibrateIcon = "$_route/vibrate_icon.png";
  static final String _blueSkyscraper = "$_route/skyscraper_blue.png";
  static final String _redSkyscraper = "$_route/skyscraper_red.png";

  get user => _user;
  get notification => _notification;
  get vibrateIcon => _vibrateIcon;
  get blueSkyscraper => _blueSkyscraper;
  get redSkyscraper => _redSkyscraper;
}
